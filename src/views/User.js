import React, { useEffect, useState } from 'react'
import Layout from '../components/layout'
//import UserList from './components/user/list'
import PetList from '../components/user/pet-list'
//import UserCreate from '../components/user/create'
import PetCreate from '../components/user/create-pet'
import jwt from 'jsonwebtoken'
import { getToken } from '../config/auth'


import {
    Switch,
    Route,
  } from "react-router-dom";

  
const User = (props) => {
const [userInfo, setUserInfo] = useState({})

useEffect(() => {
    (async () => {
        const { user} = await jwt.decode(getToken())
        setUserInfo(user)
    })()
    return () =>  {  }
}, [])


    return (
        <Layout info={userInfo}>
            <Switch>  
                <Route exact path={props.match.path + "pet-list"} component={PetList}/>
                <Route exact path={props.match.path + "create-pet"} component={PetCreate}/>
                <Route exact path={props.match.path + "edit/:id"} component={PetCreate}/>
                <Route exact path="*" component={() => (<h1>404 | Not Found</h1>)}/>      
            </Switch>
        </Layout>
    )
}

export default User
