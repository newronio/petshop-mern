import React, { useEffect, useState } from 'react'
import { ListUser } from '../../services/user'
import './user.css'

const UserList = () => {
    const [ users, setUsers ] = useState([])
    const [loading, setLoading] = useState(false)

    const getList = async () => {
       try{

        setLoading(true)
        const users = await ListUser()
        setUsers(users.data)
        setLoading(false)
       }catch(err){
        console.log('error', err)
       }
    } 

    const editUser = (user) => console.log(user)
    const deleteUser = (user) => console.log(user)

    const mountUsers = () => users.map((user, index) => (
            <tr key={index}>
            <td>{user.name}</td>
            <td>{user.email}</td>
            <td>{user.password}</td>
            <td>
                <span onClick={() => editUser(user)}>Editar</span> |
                <span onClick={() => deleteUser(user)}>Excluir</span>
            </td>
            </tr>
        ))
    
        useEffect(() => {
            getList()
        }, [])

    return (
    <section >
        <div className="list_user">
            <table border="1">
                <thead>
                    <tr>
                        <th>NAME</th>
                        <th>EMAIL</th>
                        <th>PASSWORD</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    {mountUsers()}
                </tbody>
            </table>
            {loading ? <h2>Carregando...</h2> : ""} 
        </div>
    </section>
    )
}

export default UserList