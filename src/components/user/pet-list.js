import React, { useEffect, useState } from 'react'
import { ListPet, DeletePet } from '../../services/pets'
import './pet.css'
import Loading from '../loading/loading'
import Nav from '../layout/nav/nav'
import{ useHistory } from 'react-router-dom'

const PetList = () => {
    const [ pets, setPets ] = useState([])
    const history = useHistory()

    const getList = async () => {
       try{

        const pets = await ListPet()
        setPets(pets.data)

       }catch(err){           
        console.log('error', err)
       }
    } 

    const editPet = (pet) => history.push(`/edit/${pet._id}`)
    
    const deletePet = async ({_id, typePet}) => {
        //console.log(_id)
        try{
            if(window.confirm(`Você deseja excluir o pet?: ${typePet}`)){
                await DeletePet(_id)
                getList()
            }
           
        }catch(error){
            console.log(error)
        }
    }

    const verifyIsEmpty = pets.length === 0

    const mountTable = () => {

        const rows = pets.map((pet, index) => ( 
                <tr key={index}>
                    <td>{pet.typePet}</td>
                    <td>{pet.petLocation}</td>
                    <td>{pet.petCost}</td>
                    <td>{pet.date}</td>
                    <td>
                        <span onClick={() => editPet(pet)}>Editar</span> |
                        <span onClick={() => deletePet(pet)}>Excluir</span>
                    </td>
                </tr>
            )) 

            return !verifyIsEmpty ? (
                <table border="1">
                        <thead>
                            <tr>
                                <th>TIPO</th>
                                <th>LOCALIZAÇÃO</th>
                                <th>CUSTO</th>
                                <th>DATA</th>
                                <th>ALTERAÇÕES</th>
                            </tr>
                        </thead>
                    <tbody>
                        {rows}
                    </tbody>
                 </table>
            ) : ""
        
    }
        useEffect(() => {
            getList()
        }, [])

    return (
        <React.Fragment >
            <Nav name="Novo" to="/create-pet"/>
                <div className="list_pet">
                    <Loading show={verifyIsEmpty}/>
                        {mountTable()}
                </div>
        </React.Fragment>
    )
}

export default PetList