import React, {useState} from 'react'
import { createUser } from '../../services/user.js'
import Nav from '../../components/layout/nav/nav'
import './user.css'
import{ useHistory } from 'react-router-dom'
import Layout from '../../components/layout/index'

const Create = (props) => {

const history = useHistory()
const [form, setForm] = useState({
    is_active: true,
    is_admin: false
})    


const handleChange = (event) => {
    setForm({
        ...form,
        [event.target.name]: event.target.value 
    })
    return;
}

const formIsValid = () => {
    return form.name && form.email && form.password
}

const submitForm = async () => {

    try{
        await createUser(form)
        alert('seu formulario foi enviado com sucesso')
        setForm({
            is_active: true,
            is_admin: false
        })
        history.push('/')
    }catch (err) {
        console.log('deu ruim', err.response)
    }
    
}

    return (
        <Layout >
            <Nav name="Voltar" title="Criar Usuário" to="/"/>
            <div className="create_user">
                <div className="form_login">
                        <div>
                            <label htmlFor="auth_name">Nome</label>
                            <input type="email" id="auth_name" name="name" placeholder="Insira o seu nome" onChange={handleChange} value={form.name || ""}/>
                        </div>
                        <div>
                            <label htmlFor="auth_email">E-mail</label>
                            <input type="email" id="auth_email" name="email" placeholder="Insira o seu e-mail" onChange={handleChange} value={form.email || ""} />
                        </div>
                        <div>
                            <label htmlFor="auth_email">Password</label>
                            <input type="password" id="auth_email" name="password" placeholder="Insira a sua senha" onChange={handleChange} value={form.password || ""}/>
                        </div>
                    <button disabled={!formIsValid()} onClick={submitForm}>Cadastrar</button>
                </div>
            </div>
        </Layout>
    )
}

export default Create
