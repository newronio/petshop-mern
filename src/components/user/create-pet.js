import React, {useEffect, useState} from 'react'
import { CreatePet, EditPet } from '../../services/pets.js'
import './pet.css'
import Alert from '../alert/index.js'
import Nav from '../layout/nav/nav'
import{ useHistory, useParams } from 'react-router-dom'

//import Loading from '../loading/loading'

const Create = (props) => {

const [form, setForm] = useState({})   
const [isEdit, setisEdit] = useState(false)
const [isSubmit, setSubmit] = useState(false) 
const [alert, setAlert] = useState({})
const history = useHistory()
const { id } = useParams()

useEffect(() => {

    const getShowUser = async () => {
        const pet = await EditPet(id)

        // if(user.data.senha) {        Habilitar na criação de usuário
        //     delete user.data.senha
        // }

        setForm(pet.data)
        //console.log(debt)
    }
    
    if(id) {
        setisEdit(true)
        getShowUser()
    }

}, [id])


const handleChange = (event) => {
    setForm({
        ...form,
        [event.target.name]: event.target.value 
    })
    return;
}

const formIsValid = () => {
    return form.typePet && form.petLocation && form.petCost && form.date
}

const submitForm = async () => {

    try{
        setSubmit(true)

        await CreatePet(form)

        setAlert({
            type: 'success',
            message: 'Cadastrado com sucesso',
            show: true
        })

        setSubmit(false)
         history.push('/pet-list')
        

    }catch (err) {
        setAlert({
            type: 'error',
            message: 'Ocorreu um erro',
            show: true
        })
    }
    
}

    return (
        <section >
        <Nav name="Lista" to="/pet-list"/>
            <Alert type={alert.type || ""} message={alert.message || ""} show={alert.show || false}/>

            <div className="create_pet">
                <div className="form_login">
                        <div>
                            <label htmlFor="auth_pet">Tipo de pet</label>
                            <input disabled={isSubmit} type="name" id="typePet" name="typePet" placeholder="Insira o o tipo de pet" onChange={handleChange} value={form.typePet || ""}/>
                        </div>

                        <div>
                            <label htmlFor="auth_pet">Localização do pet</label>
                            <input disabled={isSubmit} type="name" id="petLocation" name="petLocation" placeholder="Insira a localização do pet" onChange={handleChange} value={form.petLocation || ""}/>
                        </div>

                        <div>
                            <label htmlFor="auth_cost">Custo</label>
                            <input disabled={isSubmit} type="name" id="petCost" name="petCost" placeholder="Insira o valor do custo" onChange={handleChange} value={form.petCost || ""}/>
                        </div>
                        <div>
                            <label htmlFor="auth_date">Data</label>
                            <input disabled={isSubmit} type="date" id="date" name="date" onChange={handleChange} value={form.date || ""}/>
                        </div>
                    <button disabled={!formIsValid()} onClick={submitForm}>
                        {isEdit ? "Atualizar" : "Cadastrar"}
                    </button>
                    {/* <Loading show={isSubmit}/> */}
                    {isSubmit ? <div>Carregando...</div> : ""}
                </div>
            </div>
        </section>
    )
}

export default Create
