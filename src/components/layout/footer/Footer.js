import React from 'react'
import './footer.css'

import imgLogo from '../../../assets/img/pet.png'

const footer = () => {
    return (
        <footer>
            <div className="projectName">
                Petshop - Gerenciamento
            </div>
            <div className="copyRight"></div>
            <img src={imgLogo} alt="" srcset=""/>
        </footer>

    )
}

export default footer
