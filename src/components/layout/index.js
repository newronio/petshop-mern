import React from 'react'

import Header from './header/Header'
//import Menu from './components/layout/nav/nav'
import Footer from './footer/Footer'



const index = (props) => {
    //console.log(props)
    return (
        <div>
            <Header {...props} title="Gerenciador de Petshop"/>
                <main>
                    {props.children}
                </main>
            <Footer/>
        </div>
       
    )
}

export default index
