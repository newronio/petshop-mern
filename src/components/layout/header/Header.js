import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { getToken, isAuthenticated, removeToken } from '../../../config/auth'
import './header.css'

const Header = (props) => {
    const history = useHistory()
    useEffect(() => {
        if(getToken()) {
            console.log('tem token')
        }
    }, [])

    const logout = () => {
        removeToken()
        history.push('/')
    }

    const createUser = () => {
            history.push('/create-user')
    }

    return (
        <header className="d-flex">
            <div className="title">{props.title}</div>
            <div className="profile">
                {props.info ? `${props.info.name}` : ""}

                { isAuthenticated() ? <button size="sm"  color="danger" className="logout" onClick={logout}> <i className="fa fa-sign-out"></i> Sair</button> : <button size="sm"  color="danger" className="logout" onClick={createUser}> <i className="fa fa-user-circle"></i> Criar Usuário</button> }
            </div>

        </header >
    )
}


export default Header
