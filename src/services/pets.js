import { clientHttp } from '../config/config.js'

const CreatePet = (data) => clientHttp.post('/pets/', data)
const ListPet= () => clientHttp.get(`/pets`)
const DeletePet = (_id) => clientHttp.delete(`/pets/${_id}`)
const EditPet = (_id) => clientHttp.post(`/pets/${_id}`)

export {
    CreatePet,
    ListPet,
    DeletePet,
    EditPet
}