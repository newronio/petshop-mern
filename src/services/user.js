import { clientHttp } from '../config/config.js'

const createUser = (data) => clientHttp.post('/users/', data)
const ListUser = () => clientHttp.get(`/users`)
const showUserId = (_id) => clientHttp.get(`/users/${_id}`)

export {
    createUser,
    ListUser,
    showUserId
}