import axios from 'axios'
import { getToken } from './auth'

const clientHttp = axios.create({
    baseURL: 'http://localhost:5000'
})

clientHttp.defaults.headers['Content-Type'] = 'application/json'

if(getToken()){
    clientHttp.defaults.headers['x-auth-token'] = getToken()
}

export {
    clientHttp
}